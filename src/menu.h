//
// Created by S. Jansen on 8/29/19.
//

#ifndef SWITCHDIMMER_MENU_H
#define SWITCHDIMMER_MENU_H


#include <Arduino.h>

#include "ButtonsSR.h"
#include "LedsSR.h"
#include "screenTimer.h"

#define MAIN_MENU_FLASH_INTERVAL                            1500
#define SET_CURRENT_TIME_MENU_FLASH_INTERVAL                750
#define SET_DISABLE_SCREEN_START_TIME_MENU_FLASH_INTERVAL   750
#define SET_DISABLE_SCREEN_END_TIME_MENU_FLASH_INTERVAL     750

#define EXIT_MENU_BUTTON_PRESS_DURATION     5000
#define ENTER_MENU_LONG_PRESS_DURATION      5000

#define MENU_BUTTON     ((uint8_t) B00000001)        // aka EXIT_MENU_BUTTON
#define UP_BUTTON       ((uint8_t) B00000010)
#define DOWN_BUTTON     ((uint8_t) B00000100)
#define OK_BUTTON       ((uint8_t) B00001000)

bool handleMenu();
void showMainMenu();
void showSetCurrentTimeMenu();
void showSetDisableScreenStartTimeMenu();
void showSetDisableScreenEndTimeMenu();
void menuLoop();
void showCurrentMenuInputValue();


#endif //SWITCHDIMMER_MENU_H
