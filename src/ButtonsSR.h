//
// Created by S. Jansen on 12-4-2019.
//

#ifndef SWITCHDIMMER_BUTTONSSR_H
#define SWITCHDIMMER_BUTTONSSR_H

#include <Arduino.h>
#include <SPI.h>
#include "config.h"

class ButtonsSR {
  public:
    ButtonsSR(uint8_t buttonsInUse);
    void init();
    uint8_t read();
    uint8_t readWithMaxSampleTime(int sampleTime);
    void getPressedButtonsWithPressDuration(uint8_t *pressedButtons, unsigned long *pressedDuration);
    void waitTillAllReleased();

  private:
    uint8_t buttonsInUse;
    void digitalPulse(uint8_t pin, uint8_t pulseDirection);
};


#endif //SWITCHDIMMER_BUTTONSSR_H
