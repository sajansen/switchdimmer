//
// Created by samuel on 16-01-21.
//

#ifndef SWITCHDIMMER_CONFIG_H
#define SWITCHDIMMER_CONFIG_H


/** PIN MAPPING **/
// Button panel pins
#define SPI_CLK 13
#define SPI_MISO 12
#define SPI_MOSI 11
#define LOAD_BUTTONS 10
#define LOAD_LEDS 9

// Relais output pins
#define PIN_RELAIS1 2
#define PIN_RELAIS2 3
#define PIN_RELAIS3 4
#define PIN_RELAIS4 5

/** BUTTON POSITION MAPPING **/
/*
 * Below, each bit in the B00000000 byte represents the position of a button. The most right bit will be the
 * first button (following the connections of the parallel shift input register)
 */
#define MASTER_BUTTON ((uint8_t) B00000001) // Position of the master button in the buttons array
#define SCREEN_BUTTON ((uint8_t) B00000010)
#define BUTTONS_IN_USE ((uint8_t) B00011111)  // All buttons which are in use, to prevent floating non-existing buttons

// Map relais to button
#define RELAIS_1 ((uint8_t) B00000010)
#define RELAIS_2 ((uint8_t) B00000100)
#define RELAIS_3 ((uint8_t) B00001000)
#define RELAIS_4 ((uint8_t) B00010000)

/**
 * When master is turned on: all will turn on.
 * When master is turned off: all will turn off.
 * Master will turn on or off independently from the other buttons.
 */
#define MASTER_SWITCH_ALL_WITH_MASTER 0x00
/**
 * When master is pressed and any is off: all will turn on.
 * When master is pressed and any is on: all will turn off.
 * Master will turn on if any other button is on. Else it will be off.
 */
#define MASTER_SWITCH_ALL_TO_OFF_IF_ANY_ON 0x01
/**
 * When master is pressed and any is off: the previous on state will be recovered.
 * When master is pressed and any is on: all will turn off.
 * When master is long pressed: all will turn on.
 * Master will turn on if any other button is turned on. Else it will be off.
 */
#define MASTER_SWITCH_TO_LAST_SETTING_OR_ALL 0x02
#define MASTER_SWITCH_PROGRAM MASTER_SWITCH_TO_LAST_SETTING_OR_ALL  // Select what the master button must do

#define MULTIBUTTON_DETECTION_TIMEOUT 200       // (ms) time in which multiple button presses can be detected as one change
#define MASTER_SWITCH_TO_ALL_PRESS_TIME 1000    // (ms) time after when the master button has reached it's second state
#define MASTER_SWITCH_OPENS_MENU_TIME 2000    // (ms) time after when the master button has reached it's third state

#define DEBUG false   // Use of Serial monitor

#define LAST_STATE_EEPROM_ADDRESS    0x00
#define EEPROM_COMMIT_IS_AVAILABLE  false

#endif //SWITCHDIMMER_CONFIG_H
