//
// Created by S. Jansen on 8/29/19.
//

#include "menu.h"

extern ButtonsSR buttonsSR;
extern LedsSR ledsSR;

enum MenuStatus {
    UNKNOWN = 0,
    MAIN_MENU ,
    SET_CURRENT_TIME_MENU,
    SET_DISABLE_SCREEN_START_TIME_MENU,
    SET_DISABLE_SCREEN_END_TIME_MENU,
};

bool isMenuActive = false;
enum MenuStatus currentMenuStatus = UNKNOWN;
enum MenuStatus nextMenuStatus = MAIN_MENU;
uint8_t currentMenuInputValue = 0;
uint8_t currentMenuSubScreen = 0;


void flashMenuLEDs(enum MenuStatus menuStatus);

bool handleMenu() {
    if (!isMenuActive) return false;

    currentMenuStatus = UNKNOWN;
    ledsSR.fade(0x00, 500);
    delay(200);
    flashMenuLEDs(currentMenuStatus);
    delay(200);
    buttonsSR.waitTillAllReleased();
    delay(600);
    showCurrentMenuInputValue();
    while (isMenuActive) {
        menuLoop();
    }
    return true;
}

void showMainMenu() {
    isMenuActive = true;
    nextMenuStatus = MAIN_MENU;
}

void showSetCurrentTimeMenu() {
    isMenuActive = true;
    nextMenuStatus = SET_CURRENT_TIME_MENU;
}

void showSetDisableScreenStartTimeMenu() {
    isMenuActive = true;
    nextMenuStatus = SET_DISABLE_SCREEN_START_TIME_MENU;
}

void showSetDisableScreenEndTimeMenu() {
    isMenuActive = true;
    nextMenuStatus = SET_DISABLE_SCREEN_END_TIME_MENU;
}

void flashMenuLEDs(enum MenuStatus menuStatus) {
    static unsigned long menuLEDsNextFlash = 0;

    if (menuLEDsNextFlash > millis()) return;

    switch (menuStatus) {
        case MAIN_MENU:
            ledsSR.flash(0xff, 50);
            menuLEDsNextFlash = millis() + MAIN_MENU_FLASH_INTERVAL;
            break;
        case SET_CURRENT_TIME_MENU:
            ledsSR.flash(1u << (SET_CURRENT_TIME_MENU - 1u), 50);
            menuLEDsNextFlash = millis() + SET_CURRENT_TIME_MENU_FLASH_INTERVAL;
            break;
        case SET_DISABLE_SCREEN_START_TIME_MENU:
            ledsSR.flash(1u << (SET_DISABLE_SCREEN_START_TIME_MENU - 1u), 50);
            menuLEDsNextFlash = millis() + SET_DISABLE_SCREEN_START_TIME_MENU_FLASH_INTERVAL;
            break;
        case SET_DISABLE_SCREEN_END_TIME_MENU:
            ledsSR.flash(1u << (SET_DISABLE_SCREEN_END_TIME_MENU - 1u), 50);
            menuLEDsNextFlash = millis() + SET_DISABLE_SCREEN_END_TIME_MENU_FLASH_INTERVAL;
            break;
        default:
            // Unknown!
            return;
    }
    delay(100);
}

void showCurrentMenuInputValue() {
    static int8_t previousMenuInputValue = 0xff;

    if (currentMenuInputValue == previousMenuInputValue) return;
    previousMenuInputValue = currentMenuInputValue;

    ledsSR.fade(currentMenuInputValue, 0);
}

void handleMainMenuButtonsPress(uint8_t buttonsPressed, uint16_t buttonsPressedDuration) {
    if (buttonsPressed & UP_BUTTON) {
        currentMenuInputValue--;

        currentMenuInputValue = ++currentMenuInputValue % 3;

        currentMenuInputValue++;
    } else if (buttonsPressed & DOWN_BUTTON) {
        currentMenuInputValue--;

        currentMenuInputValue = --currentMenuInputValue;
        if (currentMenuInputValue > 2) currentMenuInputValue = 2;

        currentMenuInputValue++;
    } else if (buttonsPressed & OK_BUTTON) {
        nextMenuStatus = currentMenuInputValue;
    }
}

void handleSetCurrentTimeMenuButtonsPress(uint8_t buttonsPressed, uint16_t buttonsPressedDuration) {
    uint8_t maxTimeValue = currentMenuSubScreen == 0 ? 24 : 60;

    uint8_t stepSize = buttonsPressedDuration > 2000 ? 10 : 1;

    if (buttonsPressed & UP_BUTTON) {
        currentMenuInputValue = (currentMenuInputValue + stepSize) % maxTimeValue;
    } else if (buttonsPressed & DOWN_BUTTON) {
        if (buttonsPressedDuration > ENTER_MENU_LONG_PRESS_DURATION) {
            currentMenuInputValue = 0;
            return;
        }

        if (currentMenuInputValue < stepSize) {
            currentMenuInputValue += maxTimeValue;
        }
        currentMenuInputValue -= stepSize;

        if (currentMenuInputValue > maxTimeValue) {
            currentMenuInputValue = maxTimeValue;
        }

    } else if (buttonsPressed & OK_BUTTON) {
        if (currentMenuSubScreen == 0) {
            currentTime = setHours(currentTime, currentMenuInputValue);
            currentMenuInputValue = getMinutes(currentTime);
        } else if (currentMenuSubScreen == 1) {
            currentTime = setMinutes(currentTime, currentMenuInputValue);
        }
        currentMenuSubScreen++;

        if (currentMenuSubScreen > 1) {
            updateTimeToEEPROM();
            nextMenuStatus = MAIN_MENU;
        }
    }
}

// Outdated. Update with lessons learned from handleSetCurrentTimeMenuButtonsPress();
void handleSetDisableScreenStartTimeMenuButtonsPress(uint8_t buttonsPressed, uint16_t buttonsPressedDuration) {
    uint8_t maxTimeValue = currentMenuSubScreen == 0 ? 24 : 60;

    uint8_t stepSize = buttonsPressedDuration > 2000 ? 10 : 1;

    if (buttonsPressed & UP_BUTTON) {
        currentMenuInputValue = (currentMenuInputValue + stepSize) % maxTimeValue;
    } else if (buttonsPressed & DOWN_BUTTON) {
        if (currentMenuInputValue < stepSize) {
            currentMenuInputValue = maxTimeValue;
        }
        currentMenuInputValue -= stepSize;
    } else if (buttonsPressed & OK_BUTTON) {
        if (currentMenuSubScreen == 0) {
            disableScreenStartTime = setHours(disableScreenStartTime, currentMenuInputValue);
            currentMenuInputValue = getMinutes(disableScreenStartTime);
        } else if (currentMenuSubScreen == 1) {
            disableScreenStartTime = setMinutes(disableScreenStartTime, currentMenuInputValue);
        }
        currentMenuSubScreen++;

        if (currentMenuSubScreen > 1) {
            nextMenuStatus = MAIN_MENU;
        }
    }
}

// Outdated. Update with lessons learned from handleSetCurrentTimeMenuButtonsPress();
void handleSetDisableScreenEndTimeMenuButtonsPress(uint8_t buttonsPressed, uint16_t buttonsPressedDuration) {
    uint8_t maxTimeValue = currentMenuSubScreen == 0 ? 24 : 60;

    uint8_t stepSize = buttonsPressedDuration > 2000 ? 10 : 1;

    if (buttonsPressed & UP_BUTTON) {
        currentMenuInputValue = (currentMenuInputValue + stepSize) % maxTimeValue;
    } else if (buttonsPressed & DOWN_BUTTON) {
        if (currentMenuInputValue < stepSize) {
            currentMenuInputValue = maxTimeValue;
        }
        currentMenuInputValue -= stepSize;
    } else if (buttonsPressed & OK_BUTTON) {
        if (currentMenuSubScreen == 0) {
            disableScreenEndTime = setHours(disableScreenEndTime, currentMenuInputValue);
            currentMenuInputValue = getMinutes(disableScreenEndTime);
        } else if (currentMenuSubScreen == 1) {
            disableScreenEndTime = setMinutes(disableScreenEndTime, currentMenuInputValue);
        }
        currentMenuSubScreen++;

        if (currentMenuSubScreen > 1) {
            nextMenuStatus = MAIN_MENU;
        }
    }
}


void handleButtonsPress(uint8_t buttonsPressed, unsigned long buttonsPressedDuration) {
    if (buttonsPressed & MENU_BUTTON) {
        // Exit menu
        if (currentMenuStatus == MAIN_MENU || buttonsPressedDuration > EXIT_MENU_BUTTON_PRESS_DURATION) {
            isMenuActive = false;
            return;
        }

        // Go back to main menu
        nextMenuStatus = MAIN_MENU;
        return;
    }

    switch (currentMenuStatus) {
        case MAIN_MENU:
            handleMainMenuButtonsPress(buttonsPressed, buttonsPressedDuration);
            break;
        case SET_CURRENT_TIME_MENU:
            handleSetCurrentTimeMenuButtonsPress(buttonsPressed, buttonsPressedDuration);
            break;
        case SET_DISABLE_SCREEN_START_TIME_MENU:
            handleSetDisableScreenStartTimeMenuButtonsPress(buttonsPressed, buttonsPressedDuration);
            break;
        case SET_DISABLE_SCREEN_END_TIME_MENU:
            handleSetDisableScreenEndTimeMenuButtonsPress(buttonsPressed, buttonsPressedDuration);
            break;
        default:
            // Unknown!
            return;
    }
}

void handleNewMenuStatus() {
    if (currentMenuStatus == nextMenuStatus) return;
    currentMenuStatus = nextMenuStatus;

    currentMenuSubScreen = 0;
    currentMenuInputValue = 0;
    switch (nextMenuStatus) {
        case MAIN_MENU:
            currentMenuInputValue = 1;
            break;
        case SET_CURRENT_TIME_MENU:
            currentMenuInputValue = getHours(disableScreenEndTime);
            break;
        case SET_DISABLE_SCREEN_START_TIME_MENU:
            currentMenuInputValue = getHours(disableScreenStartTime);
            break;
        case SET_DISABLE_SCREEN_END_TIME_MENU:
            currentMenuInputValue = getHours(disableScreenEndTime);
            break;
        default:
            // Unknown!
            isMenuActive = false;
    }
}

void menuLoop() {
    handleNewMenuStatus();

    flashMenuLEDs(currentMenuStatus);
    showCurrentMenuInputValue();

    uint8_t buttonsPressed = 0;
    unsigned long buttonsPressedDuration = 0;
    buttonsSR.getPressedButtonsWithPressDuration(&buttonsPressed, &buttonsPressedDuration);

    if (!buttonsPressed) return;

    handleButtonsPress(buttonsPressed, buttonsPressedDuration);
    delay(200); // debounce
}