//
// Created by S. Jansen on 12-4-2019.
//

#include "LedsSR.h"

void LedsSR::digitalPulse(uint8_t pin, uint8_t pulseDirection){
  digitalWrite(pin, pulseDirection);
  digitalWrite(pin, !pulseDirection);
}

void LedsSR::init() {
  pinMode(SPI_CLK, OUTPUT);
  pinMode(LOAD_LEDS, OUTPUT);
  pinMode(SPI_MOSI, OUTPUT);

  digitalWrite(SPI_CLK, LOW);
  digitalWrite(LOAD_LEDS, LOW);
  digitalWrite(SPI_MOSI, LOW);
}

void LedsSR::transmit(uint8_t data){
  SPI.transfer(data);
  digitalPulse(LOAD_LEDS, HIGH);
}

/**
 *
 * @param to State to which must be faded
 * @param duration Duration for the fade in milliseconds
 */
void LedsSR::fade(uint8_t to, int duration){
  int pulseLength = 100;
  float dutyCycle;

  for (int i = 0; i < duration; i++){
    dutyCycle = (float) i / duration;

    // Switch off
    transmit(old_state);
    delayMicroseconds(pulseLength * (1 - dutyCycle));

    // Switch on
    transmit(to);
    delayMicroseconds(pulseLength * dutyCycle);
  }
  transmit(to); // Make sure it is switched on

  old_state = to;
}

void LedsSR::flash(uint8_t leds, int duration) {
    transmit(leds);
    delay(duration);
    transmit(old_state);
}

void LedsSR::transparentFlash(uint8_t leds, int duration) {
    transmit(old_state | leds);
    delay(duration);
    transmit(old_state);
}
