//
// Created by S. Jansen on 12-4-2019.
//

#include "ButtonsSR.h"

ButtonsSR::ButtonsSR(uint8_t buttonsInUse){
  this->buttonsInUse = buttonsInUse;
}

void ButtonsSR::digitalPulse(uint8_t pin, uint8_t pulseDirection){
  digitalWrite(pin, pulseDirection);
  digitalWrite(pin, !pulseDirection);
}

void ButtonsSR::init() {
  pinMode(SPI_CLK, OUTPUT);
  pinMode(LOAD_BUTTONS, OUTPUT);
  pinMode(SPI_MISO, INPUT);

  digitalWrite(SPI_CLK, LOW);
  digitalWrite(LOAD_BUTTONS, HIGH);
}

uint8_t ButtonsSR::read(){
  digitalPulse(LOAD_BUTTONS, LOW);
  uint8_t reading = SPI.transfer(0x0);
  return reading & buttonsInUse;
}

/**
 * Read until the reading changes to 0 or the sampleTime has passed
 * @param sampleTime
 * @return
 */
uint8_t ButtonsSR::readWithMaxSampleTime(int sampleTime){
  uint8_t reading = read();
  uint8_t sampleDelay = 100; // take sample every milliseconds

  // Don't sample when no buttons are pressed
  if (reading){
    if (sampleTime > sampleDelay) {
      long startTime = millis();
      uint8_t newReading;

      while (millis() < (startTime + sampleTime - sampleDelay)) {
        delay(sampleDelay);

        newReading = read();
        // Stop sampling when no buttons are pressed
        if (!newReading){
          break;
        }

        reading |= newReading;
      }

      // final delay. This will be a delay smaller than sampleDelay. Just to complete our sample time.
      delay(sampleTime - (millis() - startTime));
    } else if (sampleTime > 0){
      delay(sampleTime);
    }
    reading |= read();
  }

  return reading;
}

void ButtonsSR::getPressedButtonsWithPressDuration(uint8_t *pressedButtons, unsigned long *pressedDuration) {
    *pressedButtons = read();

    if (!*pressedButtons) {
        *pressedDuration = 0;
        return;
    }

    unsigned long readingStartTime = millis() - 80;
    while (read()) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(100);
        digitalWrite(LED_BUILTIN, LOW);
        delay(100);
    }
    *pressedDuration = millis() - readingStartTime;
    delay(100); // debounce
}

void ButtonsSR::waitTillAllReleased() {
  uint8_t reading = 0xff;
  while (reading) {
    reading = read();
    delay(10);
  }
}
