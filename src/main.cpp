#include "SwitchDimmer.h"

// Shift register (SN74HC165N) for the buttons
ButtonsSR buttonsSR = ButtonsSR(BUTTONS_IN_USE);

// Shift register (SN74HC595N) for the LEDs
LedsSR ledsSR = LedsSR();

uint8_t state = 0;
uint8_t lastOnState = 0x00;

void setLEDs(uint8_t state) {
    ledsSR.fade(state, 1000);
}

void initRelais() {
    pinMode(PIN_RELAIS1, OUTPUT);
    pinMode(PIN_RELAIS2, OUTPUT);
    pinMode(PIN_RELAIS3, OUTPUT);
    pinMode(PIN_RELAIS4, OUTPUT);
}

void setRelais(uint8_t state) {
    digitalWrite(PIN_RELAIS1, !(state & RELAIS_1));
    digitalWrite(PIN_RELAIS2, !(state & RELAIS_2));
    digitalWrite(PIN_RELAIS3, !(state & RELAIS_3));
    digitalWrite(PIN_RELAIS4, !(state & RELAIS_4));
}

/**
 * Fade LEDs in, een voor een
 */
void loopLED() {
    uint8_t currentState = BUTTONS_IN_USE;
    uint8_t ledState = 0x01;
    while (currentState) {
        setLEDs(ledState);
        currentState = currentState >> 1u;
        ledState |= ledState << 1u;
        delay(300);
    }
}

void saveStateToEeprom(uint8_t state) {
#if DEBUG
    Serial.print("Checking if state needs to be saved... ");
#endif
    if (EEPROM.read(LAST_STATE_EEPROM_ADDRESS) == state) {
#if DEBUG
        Serial.println("state not saved");
#endif
        return;
    }

    EEPROM.write(LAST_STATE_EEPROM_ADDRESS, state);

#if EEPROM_COMMIT_IS_AVAILABLE
    EEPROM.commit();
#endif
#if DEBUG
    Serial.println("state saved");
#endif
}

uint8_t getStateFromEeprom() {
    return EEPROM.read(LAST_STATE_EEPROM_ADDRESS);
}

void setup() {
    state = getStateFromEeprom();

    initRelais();
    setRelais(state);

    buttonsSR.init();
    ledsSR.init();

    SPI.beginTransaction(SPISettings(
            1000000, /* 4 MHz */
            MSBFIRST,
            SPI_MODE0
    ));
    SPI.begin();

#if DEBUG
    Serial.begin(9600);
#endif

    // Clear all registers by talking to them
    ledsSR.transmit(0);
    buttonsSR.read();

    // Startup message
    loopLED();
    setLEDs(state);

    initTime();

#if DEBUG
    Serial.println("Program started.");
#endif
}

void masterSwitchAllToOffIfAnyOn(const uint8_t *stateChanges, uint8_t *newState) {
    if (*stateChanges & MASTER_BUTTON) {
        // Interact on what the master button does
        if (*newState & ~(MASTER_BUTTON)) {   // Get rid of master state, it's not important
            // Turn all off
            *newState = 0x00;
        } else {
            // Turn all on
            *newState = BUTTONS_IN_USE;
        }
    } else {
        // Interact on what other buttons are doing
        if (*newState & ~MASTER_BUTTON) {
            // If any is on, set master state also to on
            *newState |= MASTER_BUTTON;
        } else {
            // If all is off, set master state also to off
            *newState = 0x00;
        }
    }
}

void masterSwitchToLastSettingOrAll(const uint8_t *stateChanges, uint8_t *newState, const uint8_t *oldState) {
    if ((uint8_t) *oldState > 0) {
        // Save last state
        lastOnState = *oldState;
    }

    if (!(*stateChanges & MASTER_BUTTON)) {
        // Interact on what other buttons are doing
        if (*newState & ~MASTER_BUTTON) {
            // If any is on, set master state also to on
            *newState |= MASTER_BUTTON;
        } else {
            // If all is off, set master state also to off
            *newState = 0x00;
        }
        return;
    }

    // Interact on what the master button does

    // Check for long press to turn all on
    int sampleTime = MASTER_SWITCH_TO_ALL_PRESS_TIME - MULTIBUTTON_DETECTION_TIMEOUT;
    int openMenuSampleTime = MASTER_SWITCH_OPENS_MENU_TIME - MULTIBUTTON_DETECTION_TIMEOUT;
    unsigned long startTime = millis();
    boolean buttonLongPress = false;
    boolean buttonLongLongPress = false;
    while (buttonsSR.read() & MASTER_BUTTON) {
        if (millis() > startTime + sampleTime) {
            buttonLongPress = true;

            if (millis() > startTime + openMenuSampleTime) {
                buttonLongLongPress = true;
                break;
            }
        }
        delay(100);
    }

    if (buttonLongLongPress) {
        *newState = *oldState;
        showMainMenu();
        return;
    }

    if (buttonLongPress) {
        // Turn all on
        *newState = BUTTONS_IN_USE;
    } else if (*newState & ~MASTER_BUTTON) {   // Get rid of master state, it's not important
        // Turn all off
        *newState = 0x00;
    } else {
        // Turn last state on
        *newState = lastOnState;
    }
}

void masterSwitchAllWithMaster(const uint8_t *stateChanges, uint8_t *newState) {
    if (*stateChanges & MASTER_BUTTON) {
        // Interact on what the master button does
        if (*newState & MASTER_BUTTON) {
            // Turn all on
            *newState = BUTTONS_IN_USE;
        } else {
            // Turn all off
            *newState = 0x00;
        }
    }
}

/**
 * Calculates the new state, given the changes and the old/current state
 * Using XOR:
 * oldState  stateChanges  |  newState
 *     0          0        |     0
 *     0          1        |     1    <-- User request for change
 *     1          0        |     1
 *     1          1        |     0    <-- User request for change
 * @param newState
 * @param oldState
 * @return
 */
uint8_t processState(const uint8_t *stateChanges, const uint8_t *oldState) {
    if ((uint8_t) *stateChanges == 0) {
        // Nothing changed
        return *oldState;
    }

    uint8_t newState = *oldState ^*stateChanges;

    // Check if MASTER button is pressed to switch ALL states
    switch (MASTER_SWITCH_PROGRAM) {
        case MASTER_SWITCH_ALL_TO_OFF_IF_ANY_ON:
            masterSwitchAllToOffIfAnyOn(stateChanges, &newState);
            break;
        case MASTER_SWITCH_TO_LAST_SETTING_OR_ALL:
            masterSwitchToLastSettingOrAll(stateChanges, &newState, oldState);
            break;
        case MASTER_SWITCH_ALL_WITH_MASTER:
        default:
            // Default behaviour is to switch ALL on if MASTER was off and vice versa
            masterSwitchAllWithMaster(stateChanges, &newState);
    }

#if DEBUG
    Serial.print("New state: ");
    Serial.println(newState, BIN);
#endif
    return newState;
}

void handleDisableScreenTimer(const uint8_t *state, uint8_t *stateChanges) {
    static unsigned long stopFlashTime = 0;

    ledsSR.transmit(*state & ~SCREEN_BUTTON);        // Turn screen button LED off for the flashes

    if (stopFlashTime == 0) {
        // Flash screen button LED for 10 seconds
        stopFlashTime = millis() + 10 * 1000;
    } else if (millis() < stopFlashTime) {
        ledsSR.transparentFlash(SCREEN_BUTTON, 100);
        delay(700);
    } else {
        // Disable screen
        *stateChanges |= SCREEN_BUTTON;
        stopFlashTime = 0;
    }
}

void loop() {
    static uint8_t oldState = 0xff;         // 0xff to force update
    static uint8_t oldStateChanges = 0xff;  // 0xff to force update
    uint8_t stateChanges = 0;
    oldState = state;

    if (tickTime()) {
        // Update EEPROM every minute with latest state
        saveStateToEeprom(state);
    }

    if (handleMenu()) {
        oldState = 0xff;
        oldStateChanges = 0xff;        // force state update
    }

    if ((state & SCREEN_BUTTON) && isItTimeToDisableScreen()) {
//        handleDisableScreenTimer(&state, &stateChanges);      // disabled because it's not working
    }

    // Get user input
    stateChanges |= buttonsSR.readWithMaxSampleTime(MULTIBUTTON_DETECTION_TIMEOUT);

    // Check for button holding
    if (oldStateChanges == stateChanges) {
        return;
    }
    oldStateChanges = stateChanges;

    // Calculate new state
    state = processState(&stateChanges, &state);

    if (state != oldState) {
        // Propagate new state
        setRelais(state);
        setLEDs(state);
    }

    // Add some debouncing
    delay(50);
}