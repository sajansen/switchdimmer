//
// Created by S. Jansen on 8/29/19.
//

#include "screenTimer.h"

uint16_t currentTime = 0;
uint16_t disableScreenStartTime = 0;
uint16_t disableScreenEndTime = 0;

uint8_t getHours(uint16_t time) {
    return time / 60;
}
uint8_t getMinutes(uint16_t time) {
    return time % 60;
}
uint16_t setHours(uint16_t time, uint8_t hours) {
    return (uint32_t) getMinutes(time) + (uint32_t) hours * 60;
}
uint16_t setMinutes(uint16_t time, uint8_t minutes) {
    return (uint32_t) getHours(time) + (uint32_t) minutes * 60;
}

bool isItTimeToDisableScreen() {
    if (disableScreenStartTime == disableScreenEndTime) return false;

    if (disableScreenStartTime < disableScreenEndTime) {
        if (currentTime > disableScreenStartTime && currentTime < disableScreenEndTime) {
            return true;
        }
    } else {
        if (currentTime > disableScreenStartTime || currentTime < disableScreenEndTime) {
            return true;
        }
    }
    return false;
}

bool tickTime() {
    static unsigned long nextTickTime = 60UL * 1000UL;
    if (millis() < nextTickTime) return false;

    nextTickTime += 60UL * 1000UL;   // Add a minute
    currentTime++;

    if (currentTime > 24 * 60) {
        currentTime = 0;
    }

    return true;
}

uint16_t getEEPROMTimeFor(int address) {
    uint8_t hours = EEPROM.read(address);
    uint8_t minutes = EEPROM.read(address + 1);
    return setHours(minutes, hours);
}

void setEEPROMTimeFor(int address, uint16_t value) {
    uint8_t hours = getHours(value);
    uint8_t minutes = getMinutes(value);
    EEPROM.update(address, hours);
    EEPROM.update(address + 1, minutes);
}

void initTime() {
    currentTime = getEEPROMTimeFor(CURRENT_TIME_EEPROM_ADDRESS);
    disableScreenStartTime = getEEPROMTimeFor(DISABLE_SCREEN_START_TIME_EEPROM_ADDRESS);
    disableScreenEndTime = getEEPROMTimeFor(DISABLE_SCREEN_END_TIME_EEPROM_ADDRESS);
}

void updateTimeToEEPROM() {
    setEEPROMTimeFor(CURRENT_TIME_EEPROM_ADDRESS, currentTime);
    setEEPROMTimeFor(DISABLE_SCREEN_START_TIME_EEPROM_ADDRESS, disableScreenStartTime);
    setEEPROMTimeFor(DISABLE_SCREEN_END_TIME_EEPROM_ADDRESS, disableScreenEndTime);
}