//
// Created by S. Jansen on 8/29/19.
//

#ifndef SWITCHDIMMER_SCREENTIMER_H
#define SWITCHDIMMER_SCREENTIMER_H

#include <Arduino.h>
#include <EEPROM.h>

#define CURRENT_TIME_EEPROM_ADDRESS                 0x01
#define DISABLE_SCREEN_START_TIME_EEPROM_ADDRESS    0x03
#define DISABLE_SCREEN_END_TIME_EEPROM_ADDRESS      0x05

extern uint16_t currentTime;        // In minutes and 24h clock
extern uint16_t disableScreenStartTime;        // In minutes and 24h clock
extern uint16_t disableScreenEndTime;        // In minutes and 24h clock

uint8_t getHours(uint16_t time);
uint8_t getMinutes(uint16_t time);
uint16_t setHours(uint16_t time, uint8_t hours);
uint16_t setMinutes(uint16_t time, uint8_t minutes);

bool isItTimeToDisableScreen();
bool tickTime();        // returns true if one minute has passed
void initTime();
void updateTimeToEEPROM();

#endif //SWITCHDIMMER_SCREENTIMER_H
