//
// Created by S. Jansen on 12-4-2019.
//

#ifndef SWITCHDIMMER_LEDSSR_H
#define SWITCHDIMMER_LEDSSR_H

#include <Arduino.h>
#include <SPI.h>
#include "config.h"

class LedsSR {
  public:
    void init();
    void transmit(uint8_t data);
    void fade(uint8_t to, int duration);
    void flash(uint8_t leds, int duration);
    void transparentFlash(uint8_t leds, int duration);

  private:
    void digitalPulse(uint8_t pin, uint8_t pulseDirection);
    uint8_t old_state = 0;
};


#endif //SWITCHDIMMER_LEDSSR_H
