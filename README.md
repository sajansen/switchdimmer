# SwitchDimmer

_Control your power plugs with a fancy button panel_

## Setup

### Hardware

Schematic version:
```
                                          [ Button Panel ]
                                                |||||
                                                |||||
                                                |||||
~ 230V ----> [ Power Supply ] --+5V--> [ Control Panel (Arduino) ]
    |                    \               |          |           |  
    |                      \+5V          |          |           |  
    |                        \           v          v           v 
     \--------->live>----------->>  [ Relais ]  [ Relais ]  [ Relais ]
      \                                  |           |           |  
       \                                 |           |           |  
        \                                v           v           v  
         \---------<neutral<-------< [ plugs ]   [ plugs ]   [ plugs ]  
```

##### Requirements
* Arduino
* 5V relais
* 74HC595 parallel out shift register
* 74HC165 parallel in shift register
* Push buttons with LED's
* Bunch of resistors
* 5V power supply
* \>=7 pole cable (to connect the Button panel to the Control panel) 
* All the (power)plugs you want

##### Installation

###### Button panel:

* Place the buttons as you want.
* Connect one terminal of the buttons to +5V.
* Connect the other terminal to the corresponding inputs of the 74HC165 chip.
* Connect a pull down resistor (~10K ohm) to each input of the 74HC165 chip.
* Connect the positive terminal of the (button's) LED's to the outputs of the 74HC595 chip with a resistor (~330 ohm) in series.
* Connect the negative terminal of the LED's to GND.
* Connect the clock signals of the shift register chips together.
* Connect these signals to an >=7 pole cable/plug:
    * SPI clock (to the shift registers)
    * MISO (to the input of 74HC165)
    * MOSI (to the input of 74HC595)
    * Load Buttons (to the load pin of 74HC165)
    * Load LED's (to the load pin/clk of 74HC595)
    * +5V
    * GND
    

###### Control panel:

* Connect the 7 pole cable from the button panel to the corresponding Arduino pins:
    * SPI clock -> SPI headerclk or pin 13
    * MISO -> SPI header MISO or pin 12
    * MOSI -> SPI header MOSI or pin 11
    * Load Buttons -> pin 10
    * Load LED's -> pin 9
    * +5V
    * GND
* Connect the relais input signal pins to the corresponding Arduino output pins.

All Arduino input and output pins can be modified in [SwitchDimmer.h](SwitchDimmer.h). Usually, the SPI pins (CLK, MISO, and MOSI) are fixed, so be careful with those.


###### Relais:

You can set these up as you like. The Arduino will control them using the signal pin, so be sure there is a common ground. 

Most relais have a 3-terminal side for the 230 V. I suggest you to connect the middle terminal to the incoming live wire. On of the other two terminals will be the NC (Normally Closed) terminal. You should connect the rest of the circuit (this can be the 230 V plug for your lights for example) to this terminal. It really doesn't matter, but it's for the user experience: a lighted LED will mean the relais - and thus the lights - is 'on'.


###### +5V Power Supply:

Connect the Arduino (and the +5V to the button panel) to the +5V output erminal of the power supply. If you have 5V relais, also connect them to this terminal. Connect all grounds together. Try to prevent ground loops ;)


### Software

Upload this project on your Arduino by opening [SwitchDimmer.ino](SwitchDimmer.ino) in the Arduino IDE. Done.

Settings can be modified in [SwitchDimmer.h](SwitchDimmer.h).


## Features

##### Menu
I'm somehow struggling to get the EEPROM to work so I can use it for an internal clock. (The EEPROM.update() command does not write the values to the EEPROM as expected.)

This internal clock will give you the option to configure when a specific relais (`SCREEN_BUTTON`) must be switched off. In order to do this, one must enter the current time and the desired switch on and off time via the menu. To enter the menu, just long press (`ENTER_MENU_LONG_PRESS_DURATION` milliseconds) the `MASTER_BUTTON`. Than after some visual LED indication, the menu will open.

To navigate through the menu and insert values, you can use these buttons:

* `MENU_BUTTON`: Go back.
* `UP_BUTTON`: Select next / increase value.  
* `DOWN_BUTTON`: Select previous / decrease value.
* `OK_BUTTON`: Execute selection / save value.

These values and buttons can be modified in [menu.h](menu.h).

##### PCB's

They are on their way! Including ability to fire an interrupt when a button is pressed. Thi can be used to sleep and wake up the Arduino.